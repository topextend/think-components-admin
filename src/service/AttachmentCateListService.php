<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-27 21:10:26
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 21:14:58
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : AttachmentCateListService.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\components\admin\service;

use think\components\admin\model\SystemAttachmentCate;
use think\components\admin\service\CommonService;
use think\admin\library\tools\TreeArray;
use think\facade\Db;

/**
 * Class AttachmentCateListService
 * @package think\components\admin\service
 */
class AttachmentCateListService extends CommonService
{
    public $is_recycle;

    public $storage_id;

    public function rules(): array
    {
        return [
            'is_recycle' => 'integer',
            'storage_id' => 'integer'
        ];
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list()
    {
        $list = SystemAttachmentCate::field("id,parent_id,name as label")->where([
            'is_deleted' => 0,
            'storage_id' => $this->storage_id ??0,
            'is_recycle' => 0,
        ])->select()->toArray();

        $list = TreeArray::arr2tree($list, "id", "parent_id",'children');
        return $this->success('success',$list);
    }
}