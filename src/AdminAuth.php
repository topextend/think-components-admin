<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-26 16:28:28
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-26 16:36:19
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : AdminAuth.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare(strict_types=1);
namespace think\components\admin;

use think\admin\Quick;
use think\admin\QuickTool;

class AdminAuth extends QuickTool
{
    public $name = "auth";

    public function resource()
    {
        return [];
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function script(): array
    {
        Quick::resourcesIn($this->getAppKey(),__DIR__);
        return [];
    }

    public function style(): array
    {
        return [];
    }
}