<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-27 21:21:40
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 21:22:58
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : AttachmentCateListAction.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare(strict_types=1);
namespace think\components\admin\actions\attachment;

use think\admin\actions\Action;
use think\admin\annotation\AdminAuth;

/**
 * 设置密码
 * @AdminAuth(title="设置密码",auth=true,login=true,menu=false)
 * @package app\admin\resource\example\actions
 */
class AttachmentCateListAction extends Action
{
    public function load()
    {
        return $this->response()->success('success', []);
    }
}