<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-27 21:08:54
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 21:08:55
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : ApiCode.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\components\admin;

class ApiCode
{
    /** @var int 成功 */
    const CODE_SUCCESS = 0;
    /** @var int 失败 */
    const CODE_ERROR = 1;
    /** @var int 未登录 */
    const CODE_NOT_LOGIN = -1;
    /** @var int app禁用 */
    const CODE_STORE_DISABLED = -2;
    /** @var int 未关注公众号 */
    const CODE_WECHAT_SUBSCRIBE = 2;
}