<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-27 21:13:42
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 21:24:19
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : SystemAttachment.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\components\admin\model;

use think\admin\http\model\Model;

/**
 * Class SystemAttachment
 * @package app\common\model
 */
class SystemAttachment extends Model
{
    protected $name = 'system_attachment';

}
