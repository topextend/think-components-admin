<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-27 21:23:34
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 21:23:34
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : SystemGroup.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types = 1);
namespace think\components\admin\model;

use think\admin\http\model\Model;

/**
 * Class SystemGroup
 *
 * @property int $id 组合数据ID
 * @property string $created_at
 * @property string $fields 数据组字段（json数据）
 * @property string $info 数据提示
 * @property string $name 数据字段名称
 * @property string $plugin_name 模块插件
 * @property string $title 数据组名称
 * @property string $updated_at
 * @package think\components\admin\model
 */
class SystemGroup extends Model
{
    protected $name = 'system_group';
}