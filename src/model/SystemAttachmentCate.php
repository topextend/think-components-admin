<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2022-12-27 21:06:10
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-12-27 21:09:20
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : SystemAttachmentCate.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2022 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);
namespace think\components\admin\model;

use think\admin\http\model\Model;

/**
 * Class SystemAttachment
 * @package think\components\admin\model
 */
class SystemAttachmentCate extends Model
{
    protected $name = 'system_attachment_cate';
}